﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public List<Cell> AliveCells
    {
        get { return _aliveCells; }
    }
    [SerializeField]
    private List<Cell> _aliveCells;

    [SerializeField]
    private Cell _cellPrefab;
    [SerializeField]
    private Transform _myPanel;
    [SerializeField]
    private int _rows = 25;
    [SerializeField]
    private int _cols = 48;
    [SerializeField]
    private Cell[,] _cells;
    [SerializeField]
    private float _speed = 1f;
    private bool _isStarting = false;
    [SerializeField]
    private Text _startStopButtonText;
    [SerializeField]
    private Button _nextButton;
    [SerializeField]
    private Slider _speedSlider;
    [SerializeField]
    private GameObject _helpPanel;
    private bool _helpOpened = false;
    private float _startDelay = 0.2f;

    void Start()
    {
        _cells = new Cell[_rows, _cols];
        _aliveCells = new List<Cell>();
        _speedSlider.value = _speedSlider.maxValue - _speed;    //inverting slider value
        _helpPanel.SetActive(false);
        GenerateGrid();
    }

    void GenerateGrid()
    {
        for (int row = 0; row < _rows; row++)
        {
            for (int col = 0; col < _cols; col++)
            {
                Cell cell = Instantiate(_cellPrefab, _myPanel);
                cell.name = "Cell_" + row.ToString() + "_" + col.ToString();
                cell.MyToggle = cell.GetComponent<Toggle>();
                _cells[row, col] = cell;
            }
        }

        CreateNeighbors();
    }

    private void CreateNeighbors()
    {
        // example of neighbors for cell #5
        // 1 2 3
        // 4   6
        // 7 8 9
        for (int row = 0; row < _rows; row++)
        {
            for (int col = 0; col < _cols; col++)
            {
                var nbrs = _cells[row, col].Neighbors;
                if (row > 0)
                {
                    if (col > 0)
                    {
                        nbrs.Add(_cells[row - 1, col - 1]);
                    }

                    nbrs.Add(_cells[row - 1, col]);
                    if (col + 1 < _cols)
                    {
                        nbrs.Add(_cells[row - 1, col + 1]);
                    }
                }

                if (col > 0)
                {
                    nbrs.Add(_cells[row, col - 1]);
                }

                if (col + 1 < _cols)
                {
                    nbrs.Add(_cells[row, col + 1]);
                }

                if (row + 1 < _rows)
                {
                    if (col > 0)
                    {
                        nbrs.Add(_cells[row + 1, col - 1]);
                    }

                    nbrs.Add(_cells[row + 1, col]);
                    if (col + 1 < _cols)
                    {
                        nbrs.Add(_cells[row + 1, col + 1]);
                    }
                }
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            StartStopOnClick();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            ResetOnClick();
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            NextStepOnClick();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_helpOpened)
            {
                HelpOnClick();
            }
            else
            {
                Application.Quit();
            }
        }
    }

    public void StartStopOnClick()
    {
        if (_isStarting)
        {
            StopGame();
        }
        else
        {
            StartGame();
        }
    }

    public void NextStepOnClick()
    {
        if (_isStarting) return;

        NextStep();
    }

    public void ResetOnClick()
    {
        foreach (Cell cell in _cells)
        {
            cell.Reset();
        }

        _aliveCells.Clear();
        StopGame();
    }

    private void StartGame()
    {
        StartCoroutine("NextStepCoroutine");
        _startStopButtonText.text = "STOP";
        _nextButton.interactable = false;
        _isStarting = true;
    }

    IEnumerator NextStepCoroutine()
    {
        yield return new WaitForSeconds(_startDelay);
        while (true)
        {
            NextStep();
            yield return new WaitForSeconds(_speed);
        }
    }

    private void NextStep()
    {
        //call Check() and Apply() only for alive cells and its neighbours

        HashSet<Cell> cellsToCheck = new HashSet<Cell>();
        foreach (Cell cell in _aliveCells)
        {
            cellsToCheck.Add(cell);
            foreach (Cell neighbour in cell.Neighbors)
            {
                cellsToCheck.Add(neighbour);
            }
        }

        foreach (Cell cell in cellsToCheck)
        {
            cell.Check();
        }

        foreach (Cell cell in cellsToCheck)
        {
            cell.Apply();
        }
    }

    private void StopGame()
    {
        StopCoroutine("NextStepCoroutine");
        _startStopButtonText.text = "START";
        _nextButton.interactable = true;
        _isStarting = false;
    }

    public void ChangeSpeed(float newSpeed)
    {
        _speed = _speedSlider.maxValue - newSpeed;      //inverting slider value
    }

    public void HelpOnClick()
    {
        _helpPanel.SetActive(!_helpOpened);
        _helpOpened = !_helpOpened;
    }
}
