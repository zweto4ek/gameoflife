using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{
    #region Properties

    public List<Cell> Neighbors
    {
        get { return _neighbors; }
        set { _neighbors = value; }
    }
    public Toggle MyToggle
    {
        get { return _myToggle; }
        set { _myToggle = value; }
    }

    #endregion
    #region Fields

    [SerializeField]
    private List<Cell> _neighbors;

    [SerializeField]
    private Toggle _myToggle;

    private GameManager _gameManager;

    [SerializeField]
    private bool _isAliveOnNextUpdate = false;
    private int _ageOnNextUpdate = 0;
    private int _age = 0;
    private int _maxAge = 4;
    private Color[] _ageColors = {Color.white, Color.green, Color.yellow, new Color(1, 0.4557f, 0, 1), Color.red};
    private Color _selectedColor = Color.magenta;
    #endregion

    private void Start()
    {
        _myToggle.onValueChanged.AddListener(delegate {
                ToggleValueChanged(_myToggle);
            });

        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (_gameManager == null)
        {
            Debug.LogError("The Game Manager is NULL.");
        }
    }

    void ToggleValueChanged(Toggle toggle)
    {
        if (!toggle.isOn)
        {
            Reset();
            _gameManager.AliveCells.Remove(this);
        }
        else
        {
            _gameManager.AliveCells.Add(this);
            toggle.graphic.color = _selectedColor;
        }
    }

    public void Check()
    {
        int aliveNeighbours = 0;
        int agedNeighbours = 0;

        foreach (Cell cell in Neighbors)
        {
            if (cell.MyToggle.isOn)
            {
                aliveNeighbours++;
                if (cell._age == _maxAge)
                {
                    agedNeighbours++;
                }
            }
        }

        if (aliveNeighbours == 3 ||                         // cell is born or stays alive when it has 3 alive neighbours
            (!MyToggle.isOn && agedNeighbours != 0) ||      // cell is born when it's dead but has at least one aged neighbour
            (MyToggle.isOn && aliveNeighbours == 2))        // cell stays alive when it's alive and has two neighbours
        {
            _isAliveOnNextUpdate = true;
            _ageOnNextUpdate = Mathf.Clamp(++_ageOnNextUpdate, 0, _maxAge);
        }
        else
        {
            _ageOnNextUpdate = 0;                           // cell dies in any other cases
            _isAliveOnNextUpdate = false;
        }
    }

    public void Apply()
    {
        MyToggle.isOn = _isAliveOnNextUpdate;
        _age = _ageOnNextUpdate;
        _myToggle.graphic.color = _ageColors[_age];
    }
    
    public void Reset()
    {
        _isAliveOnNextUpdate = false;
        _ageOnNextUpdate = 0;
        MyToggle.isOn = false;
        _age = 0;
        _myToggle.graphic.color = _ageColors[_age];
    }
}
