using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorGenerator : MonoBehaviour
{
    //[SerializeField]Color[] myColor1;
    [SerializeField]Color[] myColor1 = new Color[20];

    void Start()
    {
        //myColor1 = new Color[10];
        /*
        foreach (Color color in myColor1)
        {
            color = Color.HSVToRGB(28, 1, 100);
        }
        */
        for (int i = 0; i < myColor1.Length; i++)
        {
            //myColor1[i] = Color.HSVToRGB(28, Random.Range(1, 100), 100);
            myColor1[i] = Random.ColorHSV(0f, 1f/6, 1f, 1f, 1f, 1f, 1f, 1f);
        }
    }

    void Update()
    {
        
    }
}
